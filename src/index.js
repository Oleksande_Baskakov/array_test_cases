
function MyArray(arrayLength, ...args) {
  if (!new.target) {
    return new MyArray(arrayLength, ...args);
  }

  if (Number.isNaN(arrayLength)) {
    throw new RangeError('Invalid array length');
  }

  if (arrayLength < 0 && args.length === 0) {
    throw new RangeError('Invalid array length');
  }

  if (2 ** 32 - 1 < arrayLength + 1) {
    throw new RangeError('Invalid array length');
  }

  if (arguments.length === 1) {
    if (Number.isInteger(arrayLength) && arrayLength >= 0) {
      this.length = arrayLength;
    } else {
      this[0] = arrayLength;
      this.length = 1;
    }
  }

  if (arguments.length === 0) {
    this.length = 0;
  }

  if (args.length !== 0) {
    this[0] = arrayLength;
    this.length = 1;

    for (let i = 1; i <= args.length; i++) {
      this.length += 1;
      this[i] = args[i - 1];
    }
  }
  Object.defineProperty(this, 'length', {
    enumerable: false
  });

  return new Proxy(this, {
    set(obj, prop, value) {
      if (prop === 'length') {
        if (typeof value !== 'number') {
          throw new RangeError('Invalid array length');
        }
        else if (obj.length > value) {
          for (let i = obj.length - 1; i >= value; i--) {
            delete obj[i];
          }
        }
        obj[prop] = value;
      } else {
        obj[prop] = value;
        const newLength = Number(prop) ? Math.abs(Number(prop)) : obj.length;

        if (obj.length < newLength) {
          obj.length = newLength + 1;
        }
      }
    },

    get(obj, prop) {
      return obj[prop];
    }
  });
}

MyArray.prototype[Symbol.iterator] = function() {
  let currentInxex = 0;
  const endIndex = this.length;

  return {
    next: () => {
      if (currentInxex < endIndex) {
        const cI = currentInxex;
        currentInxex += 1;
        return {
          done: false,
          value: this[cI]
        };
      } else {
        return {
          done: true
        };
      }
    }
  };
};

MyArray.prototype.push = function(firstArg, ...args) {
  if (typeof this.length !== 'number' || !this.length) {
    this.length = 0;
  }

  if (arguments.length) {
    const index = this.length;
    this[index] = firstArg;
    this.length += 1;

    for (const el of args) {
      const index = this.length;
      this[index] = el;
      this.length += 1;
    }
  }
  return this.length;
};

MyArray.prototype.pop = function() {
  if (!this.length) {
    this.length = 0;
  }

  if (this.length === 0) {
    return;
  }

  const index = this.length - 1;
  const removedEl = this[index];
  this.length = index;

  if (this[index] === removedEl) {
    delete this[index];
  }

  return removedEl;
};

MyArray.from = function(arrayLike, ...args) {
  const newArr = new MyArray();

  for (let i = 0; i < arrayLike.length; i++) {
    newArr.push(arrayLike[i]);

    if (args[0] && args[1]) {
      newArr[i] = args[0].call(args[1], arrayLike[i]);
    } else if (args[0]) {
      newArr[i] = args[0](arrayLike[i]);
    }
  }

  return newArr;
};

MyArray.prototype.map = function(callback, ...args) {
  const newArr = new MyArray();
  const curLength = this.length;
  const context = args[0] ? args[0] : this;

  for (let i = 0; i < curLength; i++) {
    if (i in this) {
      newArr.push(callback.call(context, this[i], i, this));
    } else {
      newArr.push(this[i]);
    }
  }
  return newArr;
};

MyArray.prototype.forEach = function(callback) {
  const curLength = this.length;

  if (arguments[1]) {
    for (let i = 0; i < curLength; i++) {
      if (i in this) {
        callback.call(arguments[1], this[i], i, this);
      }
    }
  } else {
    for (let i = 0; i < curLength; i++) {
      if (i in this) {
        callback(this[i], i, this);
      }
    }
  }
};

MyArray.prototype.indexOf = function(searchElement, fromIndex = 0) {
  if (!this.length) {
    return -1;
  }

  let fromIdx = Number(fromIndex);

  if (fromIdx >= this.length) {
    return -1;
  }

  for (fromIdx; fromIdx < this.length; fromIdx++) {
    if (fromIdx in this && this[fromIdx] === searchElement) {
      return fromIdx;
    }
    fromIdx += 1;
  }
  return -1;
};

MyArray.prototype.reduce = function(func, ...args) {
  const currentLength = this.length;
  let accum = args.length ? args[0] : this[0];
  const firstIndex = args.length ? 0 : 1;

  if (!this.length && !args.length) {
    throw new TypeError();
  }

  for (let i = firstIndex; i < currentLength; i++) {
    if (i in this) {
      accum = func(accum, this[i], i, this);
    }
  }
  return accum;
};


MyArray.prototype.filter = function(callback, ...args) {
  const newArr = new MyArray();
  const currentLength = this.length;


  for (let i = 0; i < currentLength; i++) {
    const item = this[i];

    if (i in this) {
      if (callback.call(args[0], item, i, this)) {
        newArr.push(item);
      }
    }
  }
  return newArr;
};

MyArray.prototype.slice = function(begin, end) {
  const endIndex = (typeof end !== 'undefined') ? Number(end) : this.length;

  if (isNaN(endIndex)) {
    return new MyArray();
  }

  let i = 0;
  let cloned = new MyArray();
  let size = 0;
  const len = this.length;


  let start = Number(begin) || 0;
  start = (start >= 0) ? start : len + start;
  let upTo = (endIndex) ? endIndex : len;

  if (endIndex < 0) {
    upTo = len + endIndex;
  }

  size = upTo - start;
  size = size > this.length ? this.length : size;

  if (size > 0) {
    cloned = new MyArray(size);

    for (i = 0; i < size; i++) {
      cloned[i] = this[start + i];
    }
  }

  return cloned;
};

MyArray.prototype.sort = function(compareFunction) {
  for (let i = 0; i < this.length; i++) {
    for (let j = 0; j < this.length; j++) {
      if (compareFunction(this[j], this[j + 1]) > 0) {
        const swap = this[j];
        this[j] = this[j + 1];
        this[j + 1] = swap;
      }
    }
  }
  return this;
};

MyArray.prototype.toString = function() {
  let string = '';

  for (let i = 0; i < this.length; i++) {
    if (i !== (this.length - 1)) {
      string += this[i] === undefined || this[i] === null ? ',' : `${this[i]},`;
    } else {
      string += this[i] === undefined || this[i] === null ? '' : `${this[i]}`;
    }
  }
  return string;
};

MyArray.prototype.sort = function(callback) {
  const instanceMyArray = new MyArray();
  const thatArray = new MyArray(...this);
  const currLength = this.length;

  if (!callback) {
    for (let j = currLength - 1; j > 0; j--) {
      for (let i = 0; i < j; i++) {
        if (String(this[i]) > String(this[i + 1])) {
          const item = this[i];
          this[i] = this[i + 1];
          this[i + 1] = item;
        }
      }
    }
    return this;
  }

  for (let i = 1; i < currLength; i++) {
    instanceMyArray.push(callback(this[i], this[i - 1]));
  }

  for (let i = 0; i < currLength; i++) {
    if (!callback.length && instanceMyArray[i] === -1) {
      for (let i = thatArray.length - 1, j = 0; i >= 0; i -= 1, j += 1) {
        this[j] = thatArray[i];
      }
      return this;
    }
  }
};

MyArray.prototype.splice = function(start, deleteCount, ...args) {
  const newArr = new MyArray();
  const oldMass = new MyArray();
  let startIndex = start;

  if (isNaN(Number(start)) && arguments.length === 1) {
    const returnedArr = new MyArray(...this);
    this.length = 0;
    return returnedArr;
  }

  if (isNaN(start) && isNaN(deleteCount)) {
    return new MyArray();
  }

  if (typeof start !== 'number') {
    startIndex = Number(start) ? Number(start) : 0;
  }

  if (start === -Infinity) {
    startIndex = 0;
  } else {
    startIndex = start < 0 ? this.length + Math.floor(start) : Math.floor(start);
  }


  let deleteAmount = deleteCount === undefined ? this.length - startIndex : deleteCount;

  if (deleteAmount >= this.length - startIndex) {
    deleteAmount = this.length - startIndex;
  }

  for (let i = 0; i < deleteAmount; i++) {
    const deletedItem = this[startIndex + i];
    delete this[startIndex + i];
    newArr[i] = deletedItem;
    newArr.length = i + 1;

    if (i in args) {
      this[startIndex + i] = args[i];
    }
  }

  let k = 0;
  let j = 0;

  for (let i = startIndex; i < startIndex + deleteAmount; i++) {
    newArr.push(this[i]);
  }

  for (let i = startIndex + deleteAmount; i < this.length; i++) {
    oldMass.push(this[i]);
  }

  for (let i = startIndex; i < startIndex + args.length; i++) {
    this[i] = args[j];
    j += 1;
  }


  for (let i = startIndex + args.length; i < startIndex + args.length + oldMass.length; i++) {
    this[i] = oldMass[k];
    k += 1;
  }

  this.length = startIndex + args.length + oldMass.length;

  return newArr;
};

module.exports = MyArray;